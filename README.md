**Mapeo de espacios culturales**

Desarollado mediante la colaboración del [CityLAB](http://citylab.cuic.com.ar/), CUIC y el [Area Cultura](https://www.unicen.edu.ar/content/cultura-unicen) de la UNICEN

Para reutilizar el mapa seguir los siguintes pasos:

1. Descargar el archivo [csv](https://gitlab.com/cuic-unicen/citylab/mapacult/-/blob/master/planilla/Mapeo%20espacios%20culturales%20-%20Espacios.csv) e importarlo como planilla en Google Sheets. Darle permisos para que cualquier usuario con el vínculo pueda accederlo en modo lectura. La planilla debe estar localizada para México para manejar el formato de coordenada geográfica adecuadamente.

2. Ir a Herramientas / Editor de secuencias de comandos y crear el código utilizando el archivo [Geocode.gs](https://gitlab.com/cuic-unicen/citylab/mapacult/-/blob/master/planilla/geocode.gs)

3. Editar el archivo mapa.html y modificar el token de acceso al mapa: 
` mapboxgl.accessToken = '_INCLUIR EL TOKEN ACA_'; //Mapbox $

4. Modificar la variable **url** para incluír el ID de la planilla y reemplazar el parámetro sheet por el nombre de la solapa.

> url: 'https://docs.google.com/spreadsheets/d/_IDDELAPLANILLA_/gviz/tq?tqx=out:csv&sheet=_NOMBRESOLAPA_'

5. Para geolocalizar una dirección, seleccionar las columnas 'dirección, ciudad, latitud y longitud' y ejecutar desde el menú de la hoja de cálculo "Geocode" la opción "Geolocalizar celdas seleccionadas". Si las celdas latitud y longitud contienen datos, el punto se muestra en el mapa.